# VOXL FFmpeg 4.2.2

## Summary

This project is used to build and package FFmpeg 4.2.2 for use on VOXL.

This version has been modified so that the h264_omx video encoder can be used on the
Snapdragon 820. This provides hardware acceleration using OMX for H264 encoding.

Example command line:

```bash
ffmpeg -an -sn -i /dev/video2 -vcodec h264_omx -b:v 1M -f mpegts udp://192.168.0.8:4242
```

## Build

The following explains how to build FFmpeg and package as an IPK.

1. The `voxl-emulator` docker image is used to build. Please follow the voxl-docker instructions here:
https://gitlab.com/voxl-public/voxl-docker

2. On the host system, clone this repo and update the submodules to get the FFmpeg source:


```bash
# clone this repo if you haven't already
git clone https://gitlab.com/voxl-public/voxl-ffmpeg
cd voxl-ffmpeg

# and get the FFmpeg submodule
git submodule update --init --recursive
```

Now, we will build using the `voxl-emulator`.

```bash
# run the voxl-emulator docker from the top level directory
voxl-docker -i voxl-emulator

# build ffmpeg and install into ipk/data for packaging
./build.sh
```


Exit the voxl-emulator docker and make the IPK

``` bash
# exit from the voxl-emulator
exit

./make_package.sh
```

The IPK will be available in the root of the project.
